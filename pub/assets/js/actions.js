/*
*    Author: Christoph V. Schild <christoph@schild.tech>
*/

// Page has loaded
$(document).ready(function(){

    // Get all filters from server
    getFilters();

    // Add listener to "Apply Filters" button
    $("#submit_params").click(function(e){

        // Since button is in form, don't re-direct
        e.preventDefault();

        // Call performSearch() method
        performSearch();
    });

    // Sends reservation details to server
    $("#create_reservation").click(function(e) {

        // Since button is in form, don't re-direct
        e.preventDefault();

        // Convert form to array
        var new_res_details = $("#new_reservation_details").serializeArray()

        // Make GET request
        $.get("/api/v2/create_reservation", new_res_details, function(successResponse) {

            // Check if request was successful
            if(successResponse['status'] == 'success') {

                //If successful, let user know, close modal and reload table
                alert("Your reservation was successful.");
                $("#create_reservation_popup").modal('hide');
                $("#new_reservation_details")[0].reset()
                performSearch();  
            }
            else {

                // Let user know there was a problem
                alert("There was an issue with your reservation. Plese try again!");
            }
        });
    });   
});

// Gets all filters from server and build left sidebar
function getFilters() {
    $.get("/api/v2/filters", function(response) {
        Object.keys(response).forEach(category => {
            $("#generated-filters").append("<div class='filter_category'><strong class='filter_category_header'> \
            " + category + "</strong><div class='filter_options'>");
            response[category].forEach(value => {
                $("#generated-filters").append("<div class='checkbox'><label><input type='checkbox' name=' \
                " + category + "' value='" + value + "'>" + value + "</label></div>");
            });
            $("#generated-filters").append("</div></div>");
        });
    });
}

// Retrieves info about building, for popup
function getBuildingInfo(buildingID) {
    $("#building_info_popup .modal-body p").html();
    $("#building_info_popup .modal-title").html();
    $.get("/api/v2/buildingInfo", {"buildingID": buildingID}, function(buildingData){
        $("#building_info_popup .modal-title").text(buildingData['BuildingName']);
        var contentBuilder = "Building ID: " + buildingData['BuildingID'] + "<br />";
        contentBuilder += "Floors: " + buildingData['Floors'] + "<br />";
        contentBuilder += "Rooms " + buildingData['Rooms'] + "<br />";
        contentBuilder += "Address: " + buildingData['BuildingAddress'] + "<br />";

        var addressFormattedForSearch = buildingData['BuildingAddress'].replace(' ', '+');

        // gets coordinates from OSM and applies them to map
        $.get("https://nominatim.openstreetmap.org/search?q=" + addressFormattedForSearch + "&format=json", function(coords){
        var lat = coords[0]['lat'];
        var lon = coords[0]['lon'];
        contentBuilder += "<iframe width='425' height='350' frameborder='0' \
        scrolling='no' marginheight=0' marginwidth='0' \
        src='https://www.openstreetmap.org/export/embed.html?bbox=" + lon + "," + lat + 
        "&layer=mapnik' style='border: 1px solid black'></iframe>"
        $("#building_info_popup .modal-body p").html(contentBuilder);
        }) 
    });
    $("#building_info_popup").modal('show');  
}

// Get info about room and load floor plan
function getRoomInfo(roomID) {
    $("#room_info_popup .modal-body p").html();
    $("#room_info_popup .modal-title").html();
    $.get("/api/v2/roomInfo", {"roomID": roomID}, function(roomData) {
        var features = {}
        roomData.forEach(room => {
            if(room['AmenityCategory'] in features) {
                features[room['AmenityCategory']].push(room['AmenityPropertyValue']);
            }
            else {
                features[room['AmenityCategory']] = [room['AmenityPropertyValue']]
            }
        });
        $("#room_info_popup .modal-title").text(roomData[0]['BuildingName']);
        var contentBuilder = "Room ID: " + roomData[0]['RoomID'] + "<br />";
        contentBuilder += "Floor: " + roomData[0]['RoomFloor'] + "<br />";
        contentBuilder += "Building: " + roomData[0]['BuildingName'] + "<br />";
        contentBuilder += "Amenities: <br />"
        Object.keys(features).forEach(thiskey => {
            contentBuilder += thiskey + "<ul>"
            features[thiskey].forEach(value => {
                contentBuilder += "<li>" + value + "</li>"
            });
            contentBuilder += "</ul>"
        });
        $("#room-info-text").html(contentBuilder);

        // Escape URL, but inject FCB floorplan image URL into background
        $("#floor_plan").css({'background-image': 'url(\'\/assets\/images\/FCB_' + roomData[0]['RoomFloor'] + '.svg\')', 'background-size': '100%'})
    });
    $("#room_info_popup").modal('show');  
}

// Triggered when user opens "Reserve" link. Pre-populates form from multiple sources
function populateReservationDetails(reservationDetails) {
    $("#res_room").val(reservationDetails['RoomID']);
    $("#res_date").val($("#reservationDate").val());
    $("#res_start_time").val($("#reservationStartTime").val());
    $("#res_end_time").val($("#reservationEndTime").val());
    $("#create_reservation_popup").modal('show');
}

// Calls search api and populates main results table
function performSearch() {
    $("#searchResults tbody tr").remove();
    var valuesRaw = $("#filters").serializeArray();

    $.get("/api/v2/search", valuesRaw, function(searchResponse){
        searchResponse.forEach(result => {
            $("#searchResults tbody").append("<tr><td><a href='#' onclick='getRoomInfo(" + result["RoomID"] + ")'>"+ 
            result["RoomNumber"] + "</a></td><td><a href='#' onclick='getBuildingInfo(" + result["BuildingID"] + ")'>" + 
            result["BuildingName"] + "</a></td><td>" + result["Capacity"] + "</td><td><a onclick='populateReservationDetails(" + 
            JSON.stringify(result) + ")' href='#'>Reserve</a></td></tr>")
            // For the love of god, remember to stringify JSON content. Otherwise you get [Object][object]
        });
    });
}