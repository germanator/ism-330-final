/*
*    Author: Christoph V. Schild <christoph@schild.tech>
*/


const mysql = require('mysql');
const express = require('express')
const app = express()
const port = 9000

// Creates pool of mysql connections. 10 simultanoeus connections by default
var pool = mysql.createPool({
  host     : '127.0.0.1',
  user     : 'root',
  password : 'abcd',
  database : 'rooms'
});

function runQuery(inquery) {
    return new Promise( ( resolve, reject ) => {
        pool.getConnection(function(err, connection) {
            connection.query( inquery, [], ( err, rows ) => {
                if ( err ) {
                    return reject( err );
                }
                connection.release(); 
                resolve( rows );
            });
        });
    }); 
}
/*
 * Express.js routes
 */

app.get('/api/v2/filters', async (req, res) => {
    var amenities = await runQuery("SELECT AmenityCategory, AmenityPropertyValue FROM Amenities");
    var amenitiesByKey = {}
    amenities.forEach(amenity => {
        if(amenity['AmenityCategory'] in amenitiesByKey) {
            amenitiesByKey[amenity['AmenityCategory']].push(amenity['AmenityPropertyValue']);
        }
        else {
            amenitiesByKey[amenity['AmenityCategory']] = [amenity['AmenityPropertyValue']];
        }
    });
    res.send(amenitiesByKey);
});

app.get('/api/v2/search', async (req, res) => {
    paramsRaw = req.query;
    var startDate = paramsRaw["reservationDate"];
    var startTime = unescape(unescape(paramsRaw["reservationStartTime"]));
    var endTime = unescape(unescape(paramsRaw["reservationEndTime"]));
    
    delete paramsRaw.reservationDate;
    delete paramsRaw.reservationStartTime;
    delete paramsRaw.reservationEndTime;

    var queryBuilder = "SELECT DISTINCT Room.RoomID, Room.RoomNumber, Building.BuildingName, Building.BuildingID, Room.Capacity FROM Room \
    JOIN Building ON Room.BuildingID = Building.BuildingID \
    JOIN RoomAmenities ON RoomAmenities.RoomID = Room.RoomID \
    JOIN Amenities ON Amenities.AmenityID = RoomAmenities.AmenityID \
    WHERE Room.RoomID NOT IN ( \
        SELECT Reservations.ReservationRoomID FROM Reservations \
        WHERE NOT \
        (Reservations.ReservationEndTime <= '" + startDate + " " + startTime  + "' \
            OR Reservations.ReservationStartTime >= '" + startDate + " " + endTime + "' \
        )) "
    if(paramsRaw['reservationAttendees'] != "") {
        queryBuilder += " AND Room.Capacity >= " + paramsRaw["reservationAttendees"] + " ";
    }
    /* Add logic for amenity parametrs */
    delete paramsRaw.reservationAttendees;
    
    queryBuilder += " AND ("
    Object.keys(paramsRaw).forEach(param => {
        queryBuilder += "(Amenities.AmenityCategory = '" +  param + "' AND Amenities.AmenityPropertyValue IN ("
        if(Array.isArray(paramsRaw[param])) {
            paramsRaw[param].forEach(value => {
                queryBuilder += "'" + value + "',"
            });
        }
        else {
            queryBuilder += "'" + paramsRaw[param] + "',";
        }
        queryBuilder = queryBuilder.slice(0,-1) + ")) OR"
        });
        if(Object.keys(paramsRaw).length > 0) {
            queryBuilder = queryBuilder.slice(0, -3) + ");"
        }
        else {
            queryBuilder = queryBuilder.slice(0, -5) + ";"
        }
    var searchResults = await runQuery(queryBuilder);
    res.send(searchResults);
});

app.get('/api/v2/buildingInfo', async (req, res) => {
    var searchResults = await runQuery("SELECT * FROM Building WHERE BuildingID = " + req.query.buildingID);
    res.send(searchResults[0]);
});

app.get('/api/v2/roomInfo', async (req, res) => {
    queryBuilder = "SELECT Room.RoomID, Room.RoomFloor, Building.BuildingName, Amenities.* FROM Room \
    JOIN Building ON Room.BuildingID = Building.BuildingID JOIN RoomAmenities ON Room.RoomID = RoomAmenities.RoomID \
    JOIN Amenities ON RoomAmenities.AmenityID = Amenities.AmenityID WHERE Room.RoomID = " + req.query.roomID;
    var searchResults = await runQuery(queryBuilder);
    res.send(searchResults);
});
app.get('/api/v2/create_reservation', async(req, res) => {
    queryBuilder = "INSERT INTO Reservations(ReservationID, \
    ReservationRoomID, ReservationStartTime, ReservationEndTime, ReservationContact) \
    VALUES(NULL, " + req.query.res_room + ", '" + req.query.res_date + ' ' + req.query.res_start_time + "', '" + req.query.res_date + ' ' + req.query.res_end_time + "','" + req.query.res_username + "');"
    await runQuery(queryBuilder);
    res.send({'status': 'success'});
});

app.use("/", express.static(__dirname + '/pub'));
app.listen(port, '0.0.0.0')
