CREATE DATABASE rooms;
USE rooms;

CREATE TABLE Room (
RoomID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
BuildingID INT NOT NULL,
RoomNumber INT(2) NOT NULL,
RoomFloor INT(2) NOT NULL,
Capacity INT(2) NOT NULL
);

CREATE TABLE Building (
BuildingID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
BuildingName VARCHAR(80) NOT NULL,
BuildingAddress VARCHAR(150) NOT NULL,
DepartmentID INT NOT NULL,
Floors INT(2) NOT NULL,
Rooms INT(2) NOT NULL,
BuildingOpens INT NOT NULL,
BuildingCloses INT NOT NULL
);

CREATE TABLE Amenities (
AmenityID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
AmenityCategory VARCHAR(40) NOT NULL,
AmenityDescription VARCHAR(300) NOT NULL,
AmenityProperty VARCHAR(30) NOT NULL,
AmenityPropertyValue VARCHAR(30) NOT NULL
);

CREATE TABLE RoomAmenities (
JUNCTIONID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
RoomID INT NOT NULL,
AmenityID INT NOT NULL
);

CREATE TABLE Reservations (
ReservationID INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
ReservationRoomID INT NOT NULL,
ReservationStartTime DATETIME NOT NULL,
ReservationEndTime DATETIME NOT NULL,
ReservationContact VARCHAR(100) NOT NULL
);



/* Create Buildings */
INSERT INTO Building(BuildingID, BuildingName, BuildingAddress, DepartmentID, Floors, ROoms, BuildingOpens, BuildingCloses) VALUES(NULL, 'W.A. Franke College of Business', '101 E McConnell Dr, Flagstaff, AZ 86011', 1, 4, 100, 0800, 1700);
INSERT INTO Building(BuildingID, BuildingName, BuildingAddress, DepartmentID, Floors, ROoms, BuildingOpens, BuildingCloses) VALUES(NULL, 'College of Engineering, Informatics, and Applied Sciences', '2112 S Huffer Ln, Flagstaff, AZ 86011', 2, 4, 80, 0800, 1700);

/* Create Rooms */
INSERT INTO Room(RoomID, BuildingID, RoomNumber, RoomFloor, Capacity) VALUES(NULL, 1, 110, 1, 50);
INSERT INTO Room(RoomID, BuildingID, RoomNumber, RoomFloor, Capacity) VALUES(NULL, 1, 334, 3, 50);
INSERT INTO Room(RoomID, BuildingID, RoomNumber, RoomFloor, Capacity) VALUES(NULL, 1, 444, 4, 50);
INSERT INTO Room(RoomID, BuildingID, RoomNumber, RoomFloor, Capacity) VALUES(NULL, 2, 101, 1, 100);

/* Create Amenities */
    /* TVs */
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'TV', 'Television', 'size', '70inches');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'TV', 'Television', 'size', '50inches');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'TV', 'Television', 'size', '65inches');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'TV', 'Television', 'size', '50inches');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'TV', 'Television', 'size', '>70inches');
    /* Projectors */
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'Projector', 'Projection with Screen', 'brand', 'Sony');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'Projector', 'Projection with Screen', 'brand', 'Acer');
    /* Faculty Podium */
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'Podium', 'Podium for Instructor', 'connections', 'HDMI');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'Podium', 'Podium for Instructor', 'connections', 'All');
INSERT INTO Amenities(AmenityID, AmenityCategory, AmenityDescription, AmenityProperty, AmenityPropertyValue) VALUES(NULL, 'Podium', 'Podium for Instructor', 'connections', 'None');

/* Create Junctions */
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 1, 6);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 1, 9);

INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 1, 6);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 2, 6);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 2, 9);

INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 3, 6);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 3, 6);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 3, 9);

INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 4, 8);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 4, 1);
INSERT INTO RoomAmenities(JUNCTIONID, RoomID, AmenityID) VALUES(NULL, 4, 1);

/* Create Reservations*/
INSERT INTO Reservations(ReservationID, ReservationRoomID, ReservationStartTime, ReservationEndTime, ReservationContact) VALUES(NULL, 1, "2019-12-24 10:00:00", "2019-12-24 12:00:00", "Christoph Schild");
INSERT INTO Reservations(ReservationID, ReservationRoomID, ReservationStartTime, ReservationEndTime, ReservationContact) VALUES(NULL, 1, "2019-12-24 12:00:00", "2019-12-24 13:30:00", "Christoph Schild");
INSERT INTO Reservations(ReservationID, ReservationRoomID, ReservationStartTime, ReservationEndTime, ReservationContact) VALUES(NULL, 2, "2019-12-23 10:00:00", "2019-12-24 12:00:00", "Christoph Schild");
INSERT INTO Reservations(ReservationID, ReservationRoomID, ReservationStartTime, ReservationEndTime, ReservationContact) VALUES(NULL, 3, "2019-12-25 10:00:00", "2019-12-24 12:00:00", "Christoph Schild");

/*
SELECT Room.RoomFloor, Room.RoomNumber, Amenities.AmenityDescription FROM Room
    JOIN RoomAmenities ON Room.RoomID = RoomAmenities.RoomID
    JOIN Amenities ON RoomAmenities.AmenityID = Amenities.AmenityID
    WHERE Room.BuildingID = 
        (SELECT BuildingID
        FROM Building 
        WHERE BuildingName LIKE '%Franke%');


        SELECT DISTINCT Room.RoomNumber, Building.BuildingName FROM Room
        JOIN Building ON Room.BuildingID = Building.BuildingID
        JOIN RoomAmenities ON Room.RoomID = RoomAmenities.RoomID
        JOIN Amenities ON RoomAmenities.AmenityID = Amenities.AmenityID
        WHERE AmenityCategory = 'Projector' AND AmenityPropertyValue = 'Sony';


SELECT DISTINCT Room.RoomNumber, Building.BuildingName FROM Room
    JOIN Building ON Room.BuildingID = Building.BuildingID
    JOIN RoomAmenities ON RoomAmenities.RoomID = Room.RoomID
    JOIN Amenities ON Amenities.AmenityID = RoomAmenities.AmenityID
    WHERE Room.RoomID NOT IN 
    (
        SELECT Reservations.ReservationRoomID FROM Reservations
        WHERE NOT
        (
            Reservations.ReservationEndTime < '2019-12-24 11:00'
            OR
            Reservations.ReservationStartTime > '2019-12-24 10:30'
        )
    )
    AND Room.Capacity >= 100
    AND Amenities.AmenityDescription LIKE '%'
    AND Amenities.AmenityPropertyValue LIKE '%'
    ;

     SELECT DISTINCT Room.RoomNumber, Building.BuildingName, Room.Capacity FROM Room
     JOIN Building ON Room.BuildingID = Building.BuildingID
     JOIN RoomAmenities ON RoomAmenities.RoomID = Room.RoomID
     JOIN Amenities ON Amenities.AmenityID = RoomAmenities.AmenityID
     WHERE Room.RoomID NOT IN 
        (
        SELECT Reservations.ReservationRoomID FROM Reservations
        WHERE NOT
            (
            Reservations.ReservationEndTime <= '2019-12-24 10:00'
            OR Reservations.ReservationStartTime >= '2019-12-24 12:00'
            )
        )  ;
*/